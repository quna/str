#include "str/str.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[]) {
    const char *const s = "Hello World";
    str_t *str          = str_from(s, 100);

    if (strlen(s) != str_len(str)) {
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}