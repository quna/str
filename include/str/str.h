#pragma once

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

typedef struct STR str_t;

/**
 * \brief Create a new string
 *
 * \return new string instance
 */
str_t *str_new(void);

/**
 * \brief Create a new string from a c string
 *
 * \param s c string to initialize the string with
 * \param len length of source string
 * \return new string instance
 */
str_t *str_from(const char *const s, const size_t len);

/**
 * \brief destroy a string
 *
 * \param string instance
 */
void str_drop(str_t *str);

/**
 * \brief get length of the string
 *
 * \param str string instance
 * \return length of the string
 */
size_t str_len(const str_t *str);

/**
 * \brief get capacity of the string
 *
 * \param str string instance
 * \return capacity of the string
 */
size_t str_capacity(const str_t *str);

/**
 * \brief returns character string stored in the string
 *
 * \param str string instance
 * \return C character string
 */
const char *str_val(const str_t *str);

/**
 * \brief checks if a string is empty
 *
 * \param str string instance
 * \retval true string empty
 * \retval false string not empty
 */
bool str_is_empty(const str_t *str);

/**
 * \brief find first occurance of a character in a string
 *
 *
 * \param str string instance
 * \param ch character to find
 * \return -1 if character not found
 *          offset of the character otherwise
 */
int32_t str_find_char(const str_t *str, const char ch);

/**
 * \brief find first occurance of a c string in a string
 *
 * \param str string instance
 * \param substr susbstring to search for
 * \param len length of susbstring
 * \return -1 if substring not found
 *          offset of the substring otherwise
 */
int32_t str_find(const str_t *str, const char *const substr, const size_t len);

/**
 * \brief check if string contains a c string
 *
 * \param str string instance
 * \param substr susbstring to search for
 * \param len length of susbstring
 * \retval true substring found
 * \retval false substring not found
 */
bool str_contains(const str_t *str, const char *const substr, const size_t len);

/**
 * \brief check if a string starts with a c string
 *
 * \param str string instance
 * \param prefix substring to check
 * \param len length of substring
 * \return true string starts with the susbstring
 * \return false string do not start with the susbstring
 */
bool str_starts_with(const str_t *str, const char *const prefix,
                     const size_t len);

/**
 * \brief check if a string ends with a c string
 *
 * \param str string instance
 * \param suffix substring to check
 * \param len length of substring
 * \return true string ends with the susbstring
 * \return false string do not ends with the susbstring
 */
bool str_ends_with(const str_t *str, const char *const suffix,
                   const size_t len);

/**
 * \brief append a character to a string
 *
 * \param str string instance
 * \param character to append
 */
void str_push(str_t *str, const char c);

/**
 * \brief append a c string to a string
 *
 * \param str string instance
 * \param s string to append to
 * \param len length of the string to append
 */
void str_push_str(str_t *str, const char *const s, const size_t len);

/**
 * \brief append a fomatted string to a string
 *
 * \param str string instance
 * \param fmt format string
 * \param ... format string arguments
 */
void str_push_strf(str_t *str, const char *fmt, ...);

/**
 * \brief pop the last character from a string
 *
 * \param str string instance
 * \return 0 if string is empty
 *         last character of the string otherwise
 */
char str_pop(str_t *str);
