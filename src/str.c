#include "str/str.h"

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static const size_t MIN_NON_ZERO_CAP = 8;

struct STR {
    char *value;
    size_t length;
    size_t buff_size;
};

// typedef struct STR str_t;

str_t *str_new(void) {
    return str_from("", 0);
}

str_t *str_from(const char *const s, const size_t len) {
    str_t *str = malloc(sizeof(str_t));

    str->length    = strnlen(s, len);
    str->buff_size = str->length + 1;

    str->value = calloc(str->buff_size, sizeof(char));
    (void)strncpy(str->value, s, str->length);

    return str;
}

void str_drop(str_t *str) {
    free(str->value);
    free(str);
}

size_t str_len(const str_t *str) {
    return str->length;
}

size_t str_capacity(const str_t *str) {
    if (str->buff_size == 0) {
        return 0;
    }
    return str->buff_size - 1;
}

const char *str_val(const str_t *str) {
    return (const char *)(str->value);
}

bool str_is_empty(const str_t *str) {
    return (str->length == 0);
}

int32_t str_find_char(const str_t *str, const char ch) {
    for (int32_t i = 0; i < str->length; i++) {
        if (ch == str->value[i]) {
            return i;
        }
    }

    return -1;
}

int32_t str_find(const str_t *str, const char *const substr, const size_t len) {
    const size_t substr_len = strnlen(substr, len);
    if (substr_len == 0) {
        return 0;
    } else if (substr_len == 1) {
        return str_find_char(str, substr[0]);
    } else if (substr_len == str->length) {
        if (strncmp(str->value, substr, substr_len) == 0) {
            return 0;
        }
        return -1;
    } else if (substr_len > str->length) {
        return -1;
    }
    // TODO check if all the above code are required
    const char *const result = strstr(str->value, substr);
    if (result == NULL) {
        return -1;
    }
    return (result - str->value) / (sizeof(char));
}

bool str_contains(const str_t *str, const char *const substr,
                  const size_t len) {
    return str_find(str, substr, len) > 0;
}

bool str_starts_with(const str_t *str, const char *const prefix,
                     const size_t len) {
    const size_t prefix_len = strnlen(prefix, len);
    return strncmp(str->value, prefix, prefix_len) == 0;
}

bool str_ends_with(const str_t *str, const char *const suffix,
                   const size_t len) {
    const size_t suffix_len = strnlen(suffix, len);
    return str->length >= suffix_len &&  // TODO verify
           strncmp(str->value + (str->length - suffix_len), suffix,
                   suffix_len) == 0;
}

static void grow_buffer(str_t *str, const size_t additional)
{
    const size_t required_cap = str->length + additional;
    const size_t curr_cap = str->buff_size > MIN_NON_ZERO_CAP ? str->buff_size - 1 : MIN_NON_ZERO_CAP;
    size_t cap = curr_cap;
    while (required_cap > cap)
    {
        cap *= 2;
    }
    
    str->buff_size = cap + 1;
    str->value = realloc(str->value, str->buff_size);
}

void str_push(str_t *str, const char c) {
    const size_t curr_cap = str->buff_size - 1;
    if (str->length + 1 >= curr_cap) {
        grow_buffer(str, 1);
    }
    str->value[str->length] = c;
    str->length += 1;
    str->value[str->length] = '\0';
}

void str_push_str(str_t *str, const char *const s, const size_t len) {
    const size_t curr_cap   = str->buff_size - 1;
    const size_t src_len = strnlen(s, len);
    if (str->length + src_len > curr_cap) {
        grow_buffer(str, src_len);
    }
    (void)strncat(str->value, s, src_len);
    str->length += src_len;
}

void str_push_strf(str_t *str, const char *fmt, ...)
{
    va_list args;
    va_list args_copy;
    va_start(args, fmt);
    va_copy(args_copy, args);

    const size_t curr_cap = str->buff_size - 1;
    const size_t src_len = vsnprintf(NULL, 0, fmt, args);
    if (str->length + src_len > curr_cap)
    {
        grow_buffer(str, src_len);
    }

    vsprintf(str->value + str->length, fmt, args_copy);
    str->length += src_len;

    va_end(args);
    va_end(args_copy);
}

char str_pop(str_t *str) {  // TODO introduce Result data type
    if (str->length == 0) {
        return 0;
    }
    const char ch               = str->value[str->length - 1];
    str->value[str->length - 1] = '\0';
    str->length -= 1;
    return ch;
}
