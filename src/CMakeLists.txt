project(str)

option(BUILD_SHARED "Build shared libary" OFF)
if(BUILD_SHARED)
    message(STATUS "Building shared library")
    add_library(str SHARED)
else()
    message(STATUS "Building static libary")
    add_library(str STATIC)
endif()

target_sources(str 
    PRIVATE 
        str.c 
)

add_compile_options(
    -Wall -Wextra -Wpedantic -Werror -Wtypedef-redefinition
)

target_include_directories(str 
    PUBLIC 
        ${CMAKE_CURRENT_SOURCE_DIR}/../include
)
