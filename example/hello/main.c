#include "str/str.h"

#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

int println(const char *const format, ...) {
    va_list args;
    va_start(args, format);
    const int result = vprintf(format, args);
    va_end(args);
    if (result < 0) {
        return result;
    }
    const int result2 = puts("");
    if (result2 < 0) {
        return result2;
    }
    return result + result2;
}

int main(int argc, char *argv[]) {
    const char *const src_str = "hello";
    const size_t len          = 7;

    (void)println("source string:%s => source len:%lu; using len: %lu", src_str,
                  strlen(src_str), len);
    str_t *str            = str_from(src_str, len);
    const char *const val = str_val(str);
    (void)println("Length: %lu, Capacity: %lu, Value: %s", str_len(str),
                  str_capacity(str), val);
    str_push(str, 's');
    (void)println("Length: %lu, Capacity: %lu, Value: %s", str_len(str),
                  str_capacity(str), val);

    (void)println("Pop result: %c", str_pop(str));
    (void)println("Length: %lu, Capacity: %lu, Value: %s", str_len(str),
                  str_capacity(str), val);

    str_push_str(str, " world", 13);
    (void)println("Length: %lu, Capacity: %lu, Value: %s", str_len(str),
                  str_capacity(str), val);

    (void)println(
        "has suffix 'world' : %s",
        str_ends_with(str, "worlds", strlen("worlds")) ? "true" : "false");
    (void)println("contains 'blah': %s",
                  str_contains(str, "blah", 76) ? "true" : "false");
    (void)println("contains 'wo', %s",
                  str_contains(str, "wo", 76) ? "true" : "false");
    (void)println("ends with 'ld': %s",
                  str_ends_with(str, "ld", 4) ? "true" : "false");

    time_t t     = time(NULL);
    struct tm tm = *localtime(&t);
    str_push_strf(str, ", %d-%02d-%02d %02d:%02d:%02d.", tm.tm_year + 1900,
                  tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
    (void)println("%s\nLength:%zu \t Capacity:%zu", str_val(str), str_len(str),
                  str_capacity(str));

    str_drop(str);

    const str_t *const heystack = str_from("Hello world", 30);
    const char *const needle    = "world";
    const char *const needle2   = "worlds";
    const int32_t needle_index  = str_find(heystack, needle, strlen(needle));
    const int32_t needle2_index = str_find(heystack, needle2, strlen(needle2));

    (void)printf("Starting index of '%s' in '%s': %d\n", needle,
                 str_val(heystack), needle_index);
    (void)printf("Starting index of '%s' in '%s': %d\n", needle2,
                 str_val(heystack), needle2_index);

    return EXIT_SUCCESS;
}