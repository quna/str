#include "str/str.h"

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[]) {
    if (argc == 1) {
        (void)printf("Usage: %s <space seperated string list>\n", argv[0]);
        return EXIT_FAILURE;
    }

    str_t *str = str_new();

    int str_count = 1;
    while (true) {
        str_push_str(str, argv[str_count], strlen(argv[str_count]));
        str_count++;
        if (str_count == argc) {
            break;
        }
        str_push(str, ' ');
    }

    (void)printf("Space joined string: '%s'\n", str_val(str));

    str_drop(str);
    return EXIT_SUCCESS;
}